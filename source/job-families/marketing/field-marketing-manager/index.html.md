---
layout: job_family_page
title: "Field Marketing Manager"
---

GitLab is looking for a highly motivated, sales-focused field marketer to support our Sales Teams. The Field Marketing Manager position is responsible for all regional marketing supporting sales in their specific region.

The Field Marketing Manager - US-West is responsible for supporting our US West Coast Sales Team.

The Field Marketing Manager - US-East is responsible for supporting our US East Coast. 

The Field Marketing Manager - US-Public Sector is responsible for supporting our US Public Sector team. 

The Field Marketing Manager - EMEA is responsible for supporting our Europe, Middle East, and Africa (EMEA) sales team.

The Field Marketing Manager - APAC is responsible for supporting our Asia Pacific (APAC) sales team.

A successful Field Marketing Manager has strong understanding of sales-focused marketing as well as our audiences of enterprise IT leaders, IT ops practitioners, and developers. They enjoy taking charge of regional marketing programs, detailed planning, proactive communication, and flawlessly delivering memorable marketing experiences that support our sales objectives.

## Responsibilities

* Adapt digital and content marketing programs to the needs of a regional sales team.
* Create, expand, and accelerate sales opportunities through regional and account-based marketing execution, within marketing defined strategy.
* Be an advocate for the sales region you support and help the rest of the marketing department understand their priorities.
* Be an advocate for the marketing department and help the sales team you support understand the marketing department's priorities.
* Swag management for sponsored events, GitLab owned events, and in support of the sales department.
* Decision making and discretion regarding event selection and planning in support of regional sales objectives.
* Regional event strategy, decision making, and onsite management.
* Event logistics in support of the team. From helping to book space for meetings to making sure the booth is staffed, and making sure every aspect of our events are well organized.

## Requirements
- Past experience delivering, accelerating, and expanding sales pipeline through regional marketing.
- 5+ years of experience.
- Strong communication skills without a fear of over communication. This role will require effective collaboration and coordination across internal and external stakeholders.
- Ability to empathize with the needs and experiences of IT leaders, IT ops practicioners, and developers.
- Extremely detail-oriented and organized, and able to meet deadlines.
- You share our [values](/handbook/values/), and work in accordance with those values.
- A passion and strong understanding of the developer tools, IT operations tools, and/or IT security markets.
- Experience with supporting both direct sales and channel sales teams.

## Additional Requirements for EMEA
- Past experience running marketing to develop DACH, BENELUX, Nordics, and/or UK & Ireland.
- Strong understanding of marketing to the automotive, banking, and tech sectors.

## Additional Requirements for US Public Sector 
- Successful track record working with US Public Sector sales team,  distributors, and channel partners. 
- An understanding of state & local, Civilian, DoD, and Intelligence Community agencies and ability to translate this into a cohesive marketing plan for the Public Sector. 

# Manager, North America Field Marketing
As GitLab grows, it will be important to have regional leadership to scale the field marketing activities. The Manager, North America Field Marketing, is a leadership role responsible for managing field marketers in North America, including both commercial and public sector activities. The Manager, North America Field Marketing will report to the Chief Marketing Officer.

## Responsibilities
- Manage a team of field marketers to develop and execute a regional demand generation strategy in line with company sales goals.
- Represent field marketing with field sales peers and management.
- Collaborate with senior marketing leaders, finance and sales leaders to create integrated plans and budgets.
- Be a player/coach, executing and managing marketing activities.
- Hire exceptional field marketers who align with GitLab values.

## Requirements
- The manager role includes all of the requirements above, plus:
- 10+ years of marketing experience
- Strong communication skills with senior marketing and sales leaders.
- Experience managing direct or cross-functional teams of 3-10 marketers.
- Experience managing a multi-million dollar marketing budget and working with senior finance and marketing leaders on annual budgeting processes.
- Demonstrated track record of driving results and staying within a budget.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).

- Qualified candidates will be invited to schedule a [screening call](/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters.
- Next, candidates will be invited to schedule a series of 45 minute interviews with the CMO, Director of Field Marketing, regional sales leader, and a seller who he/she would support. 
- Depending on location, candidates may meet in person with any of the above. 
- Finally, our CEO may choose to conduct a final interview.
- Successful candidates will subsequently be made an offer via email. The total compensation for this role listed in https://about.gitlab.com/job-families/marketing/field-marketing-manager/ is 100% base salary. 

Additional details about our process can be found on our [hiring page](/handbook/hiring/).
